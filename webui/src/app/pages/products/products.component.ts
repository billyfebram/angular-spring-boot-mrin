import { Component, TemplateRef, ViewChild, OnInit, ComponentRef, ViewContainerRef } from '@angular/core';
import { ProductService } from '../../services/api/product.service';
import { Router } from '@angular/router';
import { IModalDialog, IModalDialogButton, IModalDialogOptions, ModalDialogService } from 'ngx-modal-dialog';
import { FormBuilder } from '@angular/forms';

@Component({
    // tslint:disable-next-line: component-selector
    selector   : 's-products-modal-pg',
    templateUrl: './products-modal.component.html',
    styleUrls  : [ './products.scss'],
  })
  export class ProductModalComponent implements IModalDialog {
    actionButtons: IModalDialogButton[];
    editForm = this.fb.group({
        id: [],
        productName: [],
        productCode: [],
        description: [],
        standardCost: [],
        listPrice: [],
        targetLevel: [],
        reorderLevel: [],
        minimumReorderQuantity: [],
        quantityPerUnit: [],
        discontinued: [],
        category:[]
    });
    productComponent: any;
  
    constructor(private productService: ProductService, private fb: FormBuilder) {
      this.actionButtons = [
        { text: 'Close' },
        { text: 'Save', onAction: () => {
          this.productComponent.product = {
            id: this.editForm.get(['id']).value,
            productName: this.editForm.get(['productName']).value,
            productCode: this.editForm.get(['productCode']).value,
            description: this.editForm.get(['description']).value,
            standardCost: this.editForm.get(['standardCost']).value,
            listPrice: this.editForm.get(['listPrice']).value,
            targetLevel: this.editForm.get(['targetLevel']).value,
            reorderLevel: this.editForm.get(['reorderLevel']).value,
            minimumReorderQuantity: this.editForm.get(['minimumReorderQuantity']).value,
            quantityPerUnit: this.editForm.get(['quantityPerUnit']).value,
            discontinued: this.editForm.get(['discontinued']).value,
            category: this.editForm.get(['category']).value
          };

          if (this.productComponent.product.id == null) {
            productService.save(this.productComponent.product).subscribe((res) => {
              alert('Save success');
              this.productComponent.getPolicyData();
              return true;
            });
          } else {
            productService.update(this.productComponent.product).subscribe((res) => {
              alert('Update success');
              this.productComponent.getPolicyData();
              return true;
            });
          }
        }}
        // { text: 'Close' }, // no special processing here
        // { text: 'I will always close', onAction: () => true },
        // { text: 'I never close', onAction: () => false }
      ];
    }

    dialogInit(reference: ComponentRef<IModalDialog>, options: Partial<IModalDialogOptions<any>>) {
        // no processing needed
        this.productComponent = options.data;
    
        this.editForm.patchValue({
          id: this.productComponent.product.id,
          productCode: this.productComponent.product.productCode,
          productName: this.productComponent.product.productName,
            description: this.productComponent.product.description,
            standardCost: this.productComponent.product.standardCost,
            listPrice: this.productComponent.product.listPrice,
            targetLevel: this.productComponent.product.targetLevel,
            reorderLevel: this.productComponent.product.reorderLevel,
            minimumReorderQuantity: this.productComponent.product.minimumReorderQuantity,
            quantityPerUnit: this.productComponent.product.quantityPerUnit,
            discontinued: this.productComponent.product.discontinued,
            category: this.productComponent.product.category
        });
      }
    
      onClose() {
        console.log('DIALOG CLOSED');
      }
}

@Component({
	selector: 's-products-pg',
	templateUrl: './products.component.html',
    styleUrls: [ './products.scss'],
})
export class ProductsComponent implements OnInit {

    @ViewChild('productDiscontinuedTpl') productDiscontinuedTpl: TemplateRef<any>;
    @ViewChild('productActionTpl') productActionTpl: TemplateRef<any>;

    //ngx-Datatable Variables
    columns:any[];
    rows:any[];
    product: any;

    constructor( private router: Router,  private modalService: ModalDialogService,
        private viewRef: ViewContainerRef, private productService: ProductService) {}
    ngOnInit() {
        var me = this;
        me.getPolicyData();
        this.columns=[
            {prop:"productCode"  , name: "Code"         , width:60  },
            {prop:"productName"  , name: "Name"         , width:200 },
            {prop:"standardCost" , name: "Standard Cost", width:100 },
            {prop:"listPrice"    , name: "List Price"   , width:100 },
            {prop:"category"     , name: "Category"     , width:100 },
            {prop:"targetLevel"  , name: "Target Level" , width:100 },
            {prop:"reorderLevel" , name: "Reorder Level", width:100 },
            {prop:"minimumReorderQuantity", name: "Min Order", width:100 },
            {prop:"discontinued" , name: "Discontinued" , width:90, cellTemplate: this.productDiscontinuedTpl},
            {prop:"action", name: "Action", width:100, cellTemplate: this.productActionTpl}
        ];

    }

    getPolicyData() {
        this.productService.getProducts().subscribe( (policyData) => {
            this.rows = policyData;
        });
    }

    delete(row) {
        let that = this;
        this.productService.delete(row.id).subscribe((res) => {
            alert('Sukses delete');
            that.getPolicyData();
        });
        console.log(row);
    }

    openNewDialog() {
        this.modalService.openDialog(this.viewRef, {
          title: 'Create or Update Product',
          childComponent: ProductModalComponent,
          data: this
        });
    }

    create() {
        this.product = {};
        this.openNewDialog();
    }
    edit(row) {
        this.product = row;
        this.openNewDialog();
    }
}
