import { Component, OnInit,TemplateRef, ViewChild, ComponentRef, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../../services/api/employee.service';
import { IModalDialog, IModalDialogButton, IModalDialogOptions, ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { FormBuilder } from '@angular/forms';

@Component({
  // tslint:disable-next-line: component-selector
  selector   : 's-employees-modal-pg',
  templateUrl: './employees-modal.component.html',
  styleUrls  : [ './employees.scss'],
})
export class EmployeeModalComponent implements IModalDialog {
  actionButtons: IModalDialogButton[];
  editForm = this.fb.group({
    id: [],
    firstName: [],
    lastName: [],
    email: [],
    phone: [],
    department: [],
  });
  employeeComponent: any;

  constructor(private employeeService: EmployeeService, private fb: FormBuilder) {
    this.actionButtons = [
      { text: 'Close' },
      { text: 'Save', onAction: () => {
        this.employeeComponent.employee = {
          id: this.editForm.get(['id']).value,
          lastName: this.editForm.get(['lastName']).value,
          firstName: this.editForm.get(['firstName']).value,
          email: this.editForm.get(['email']).value,
          phone: this.editForm.get(['phone']).value,
          department: this.editForm.get(['department']).value
        };

        if (this.employeeComponent.employee.id == null) {
          employeeService.save(this.employeeComponent.employee).subscribe((res) => {
            alert('Save success');
            this.employeeComponent.getPageData();
            return true;
          });
        } else {
          employeeService.update(this.employeeComponent.employee).subscribe((res) => {
            alert('Update success');
            this.employeeComponent.getPageData();
            return true;
          });
        }
      }}
      // { text: 'Close' }, // no special processing here
      // { text: 'I will always close', onAction: () => true },
      // { text: 'I never close', onAction: () => false }
    ];
  }

  dialogInit(reference: ComponentRef<IModalDialog>, options: Partial<IModalDialogOptions<any>>) {
    // no processing needed
    this.employeeComponent = options.data;

    this.editForm.patchValue({
      id: this.employeeComponent.employee.id,
      firstName: this.employeeComponent.employee.firstName,
      lastName: this.employeeComponent.employee.lastName,
      phone: this.employeeComponent.employee.phone,
      email: this.employeeComponent.employee.email,
      department: this.employeeComponent.employee.department,
    });
  }

  onClose() {
    console.log('DIALOG CLOSED');
  }
}

@Component({
  // tslint:disable-next-line: component-selector
  selector   : 's-employees-pg',
  templateUrl: './employees.component.html',
  styleUrls  : [ './employees.scss'],
})
export class EmployeesComponent implements OnInit {
    @ViewChild('employeeActionTpl') employeeActionTpl: TemplateRef<any>;

    columns: any[];
    rows: any[];
    employee: any;

    constructor(
      private router: Router,
      private modalService: ModalDialogService,
      private viewRef: ViewContainerRef, private employeeService: EmployeeService) { }

    openNewDialog() {
      this.modalService.openDialog(this.viewRef, {
        title: 'Create or Update Employee',
        childComponent: EmployeeModalComponent,
        data: this
      });
    }

    ngOnInit() {
        const me = this;
        me.getPageData();

        this.employee = {};
        this.columns=[
            {prop:"id"        , name: "ID"          , width:50  },
            {prop:"firstName" , name: "First Name"  , width:120 },
            {prop:"lastName"  , name: "Last Name"   , width:120 },
            {prop:"email"     , name: "Email"       , width:250 },
            {prop:"phone"     , name: "Phone"       , width:160 },
            {prop:"department", name: "Department"  , width:220 },
            {prop:"action", name: "Action", width:100, cellTemplate: this.employeeActionTpl}

        ];
    }

    getPageData() {
        var me = this;
        this.employeeService.getEmployees().subscribe((data) => {
            this.rows = data.items;
        });
    }

    delete(row) {
        const that = this;
        console.log(row);
        this.employeeService.delete(row.id).subscribe((res) => {
            alert('Sukses delete');
            that.getPageData();
        });
    }

    create() {
      this.employee = {};
      this.openNewDialog();
    }
    edit(row) {
      this.employee = row;
      this.openNewDialog();
    }
}
