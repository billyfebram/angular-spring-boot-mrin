import { Component, OnInit,TemplateRef, ViewChild,HostListener, ViewContainerRef, ComponentRef } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../../services/api/customer.service';
import { ModalDialogService, IModalDialog, IModalDialogButton, IModalDialogOptions } from 'ngx-modal-dialog';
import { EmployeeModalComponent } from '../employees/employees.component';
import { EmployeeService } from 'app/services/api/employee.service';
import { FormBuilder } from '@angular/forms';

@Component({
    // tslint:disable-next-line: component-selector
    selector   : 's-customers-modal-pg',
    templateUrl: './customers-modal.component.html',
    styleUrls  : [ './customers.scss'],
  })
  export class CustomerModalComponent implements IModalDialog {
    actionButtons: IModalDialogButton[];
    editForm = this.fb.group({
      id: [],
      firstName: [],
      lastName: [],
      email: [],
      phone: [],
      company: [],
      address: []
    });
    customerComponent: any;

    constructor(private customerService: CustomerService, private fb: FormBuilder) {
      this.actionButtons = [
        { text: 'Close' },
        { text: 'Save', onAction: () => {
          this.customerComponent.customer = {
            id: this.editForm.get(['id']).value,
            lastName: this.editForm.get(['lastName']).value,
            firstName: this.editForm.get(['firstName']).value,
            email: this.editForm.get(['email']).value,
            phone: this.editForm.get(['phone']).value,
            address: this.editForm.get(['address']).value,
            company: this.editForm.get(['company']).value
          };

          if (this.customerComponent.customer.id == null) {
            customerService.save(this.customerComponent.customer).subscribe((res) => {
              alert('Save success');
              this.customerComponent.getPageData();
              return true;
            });
          } else {
            customerService.update(this.customerComponent.customer).subscribe((res) => {
              alert('Update success');
              this.customerComponent.getPageData();
              return true;
            });
          }
        }}
        // { text: 'Close' }, // no special processing here
        // { text: 'I will always close', onAction: () => true },
        // { text: 'I never close', onAction: () => false }
      ];
    }
  
    dialogInit(reference: ComponentRef<IModalDialog>, options: Partial<IModalDialogOptions<any>>) {
      // no processing needed
      this.customerComponent = options.data;
  
      this.editForm.patchValue({
        id: this.customerComponent.customer.id,
        firstName: this.customerComponent.customer.firstName,
        lastName: this.customerComponent.customer.lastName,
        phone: this.customerComponent.customer.phone,
        email: this.customerComponent.customer.email,
        address: this.customerComponent.customer.address,
        company: this.customerComponent.customer.company
      });
    }
  
    onClose() {
      console.log('DIALOG CLOSED');
    }
  }

@Component({
	selector: 's-customers-pg',
	templateUrl: './customers.component.html',
    styleUrls: [ './customers.scss'],
})
export class CustomersComponent implements OnInit {

    @ViewChild('customerActionTpl') customerActionTpl: TemplateRef<any>;


    columns:any[];
    rows:any[];
    pageSize:number=10;
    currentPage:number=0;
    isLastPageLoaded:boolean=false;
    isLoading:boolean=false;
    customer: any;

    constructor(private router: Router, 
        private modalService: ModalDialogService,
        private viewRef: ViewContainerRef, 
        private customerService: CustomerService) { }

    ngOnInit() {
        let me = this;
        me.getPageData();

        this.columns=[
            {prop:"id"       , name: "ID"          , width:50  },
            {prop:"firstName", name: "First Name"  , width:120 },
            {prop:"lastName" , name: "Last Name"   , width:120 },
            {prop:"company"  , name: "Company"     , width:120 },
            {prop:"email"    , name: "Email"       , width:200 },
            {prop:"phone"    , name: "Phone"       , width:160 },
            {prop:"address"  , name: "Address"     , width:220 },
            {prop:"action", name: "Action", width:100, cellTemplate: this.customerActionTpl}
        ];
    }

    getPageData(isAppend:boolean=false) {

        if (this.isLastPageLoaded===false){
            let me = this;
            me.isLoading=true;
            this.customerService.getCustomers(this.currentPage,this.pageSize).subscribe((data) => {
                me.isLastPageLoaded=data.last;
                me.currentPage = data.currentPageNumber+1;
                if (isAppend===true){
                    me.rows = me.rows.concat(data.items);
                }
                else{
                    me.rows = data.items;
                }
                me.isLoading=false;
            });
        }
    }

    onScroll() {
        console.log("bottom")
        if (this.isLoading===false){
            this.getPageData(true);
        }
    }

    delete(row) {
        const that = this;
        console.log(row);
        this.customerService.delete(row.id).subscribe((res) => {
            alert('Sukses delete');
            that.getPageData();
        });
    }

    openNewDialog() {
        this.modalService.openDialog(this.viewRef, {
            title: 'Create or Update Customers',
            childComponent: CustomerModalComponent,
            data: this
        });
    }

    create() {
        this.customer = {};
        this.openNewDialog();
    }
    edit(row) {
        this.customer = row;
        this.openNewDialog();
    }
}
