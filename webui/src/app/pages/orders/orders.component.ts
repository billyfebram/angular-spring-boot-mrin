import { EmployeeService } from './../../services/api/employee.service';
import { Component, OnInit,TemplateRef, ViewChild, ComponentRef, ViewContainerRef } from '@angular/core';
import { OrderService } from '../../services/api/order.service';
import { Router } from '@angular/router';
import 'rxjs/add/operator/mergeMap';
import { IModalDialog, IModalDialogButton, IModalDialogOptions, ModalDialogService } from 'ngx-modal-dialog';
import { FormBuilder } from '@angular/forms';
import { CustomerService } from 'app/services/api/customer.service';

@Component({
    // tslint:disable-next-line: component-selector
    selector   : 's-orders-modal-pg',
    templateUrl: './orders-modal.component.html',
    styleUrls  : [ './orders.scss'],
  })
  export class OrderModalComponent implements IModalDialog {
    actionButtons: IModalDialogButton[];
    editForm = this.fb.group({
      id: [],
      orderDate: [],
      shippedDate: [],
      paidDate: [],
      paymentType: [],
      orderStatus: [],
      customerId: [],
      employeeId: []
    });
    orderComponent: any;
    customers: any[];
    employees: any[];
    constructor(private orderService: OrderService,
        private customerService: CustomerService,  
        private employeeService: EmployeeService, private fb: FormBuilder) {
      this.actionButtons = [
        { text: 'Close' },
        { text: 'Save', onAction: () => {
          this.orderComponent.order = {
            id: this.editForm.get(['id']).value,
            orderDate: this.editForm.get(['orderDate']).value,
            shippedDate: this.editForm.get(['shippedDate']).value,
            paidDate: this.editForm.get(['paidDate']).value,
            paymentType: this.editForm.get(['paymentType']).value,
            orderStatus: this.editForm.get(['orderStatus']).value,
            customerId: this.editForm.get(['customerId']).value,
            employeeId: this.editForm.get(['employeeId']).value
          };

          if (this.orderComponent.order.id == null) {
            orderService.save(this.orderComponent.order).subscribe((res) => {
              alert('Save success');
              this.orderComponent.getPageData();
              return true;
            });
          } else {
            orderService.update(this.orderComponent.order).subscribe((res) => {
              alert('Update success');
              this.orderComponent.getPageData();
              return true;
            });
          }
        }}
        // { text: 'Close' }, // no special processing here
        // { text: 'I will always close', onAction: () => true },
        // { text: 'I never close', onAction: () => false }
      ];
    }
  
    dialogInit(reference: ComponentRef<IModalDialog>, options: Partial<IModalDialogOptions<any>>) {
      // no processing needed
      this.orderComponent = options.data;
  
      const that = this;
      this.customerService.getCustomers(0,100).subscribe((data) => {
        that.customers = data.items;
      });

      this.employeeService.getEmployees(0,100).subscribe((data) => {
        that.employees = data.items;
      });
      this.editForm.patchValue({
        id: this.orderComponent.order.id,
        orderDate: JSON.stringify(new Date(this.orderComponent.order.orderDate)).slice(1,11),
        shippedDate: JSON.stringify(new Date(this.orderComponent.order.shippedDate)).slice(1,11),
        paidDate: JSON.stringify(new Date(this.orderComponent.order.paidDate)).slice(1,11),
        paymentType: this.orderComponent.order.paymentType,
        orderStatus: this.orderComponent.order.orderStatus,
        customerId: this.orderComponent.order.customerId,
        employeeId: this.orderComponent.order.employeeId,
      });
    }
  
    onClose() {
      console.log('DIALOG CLOSED');
    }
  }

@Component({
	selector: 's-orders-pg',
	templateUrl: './orders.component.html',
    styleUrls: [ './orders.scss'],
})
export class OrdersComponent implements OnInit {
    @ViewChild('orderStatusCellTpl') statusCellTpl: TemplateRef<any>;
    @ViewChild('orderIdTpl') orderIdTpl: TemplateRef<any>;
    @ViewChild('orderActionTpl') orderActionTpl: TemplateRef<any>;

    columns:any[];
    rows:any[];
    orderByStatusData: any[] = [];
    isLoading:boolean=false;
    order: any;
    constructor(private router: Router,  private modalService: ModalDialogService,
        private viewRef: ViewContainerRef, private orderService: OrderService) { }

    ngOnInit() {
        var me = this;
        me.getPageData();
        this.columns=[
            {prop:"orderId"         , name: "ID"           , width:65, cellTemplate: this.orderIdTpl   },
            {prop:"orderDate"       , name: "Order Date"   , width:105 },
            {prop:"orderStatus"     , name: "Status"       , width:85, cellTemplate: this.statusCellTpl },
            {prop:"orderName"    , name: "Name"         , width:150 },
            {prop:"orderEmail"   , name: "Email"        , width:200 },
            {prop:"orderCompany" , name: "Company"      , width:110 },
            {prop:"paymentType"     , name: "Pay Type"     , width:80  },
            {prop:"paidDate"        , name: "Pay Date"     , width:105 },
            {prop:"shippedDate"     , name: "Ship Date"    , width:105 },
            {prop:"shipCountry"     , name: "Ship Country" , width:110 },
            {prop:"action", name: "Action", width:300, cellTemplate: this.orderActionTpl}
        ];
    }

    getPageData() {
        var me = this;
        let legendColors = {"On Hold":'#ef2e2e', "Shipped":'#ff8e28', "Complete":'#61c673', "New":'#007cbb'};
        me.isLoading=true;
        me.orderService.getOrderStats("status")
        .mergeMap(function(statusData){
            me.orderByStatusData = statusData.items.map(function(v,i,a){
                return {name:v.name, value:v.value, color:legendColors[v.name]}
            });
            console.log("Got Order Stats");
            return me.orderService.getOrderInfo();
        })
        .subscribe(function(orderData){
            me.rows = orderData;
            me.isLoading=false;
            console.log("Got Order Data");
        })
    }

    delete(row) {
        const that = this;
        this.orderService.delete(row.orderId).subscribe((res) => {
            alert('Sukses delete');
            that.getPageData();
        });
    }

    openNewDialog() {
        this.modalService.openDialog(this.viewRef, {
            title: 'Create or Update Orders',
            childComponent: OrderModalComponent,
            data: this
        });
    }

    create() {
        this.order = {};
        this.openNewDialog();
    }
    edit(row) {
        this.order = row;
        this.openNewDialog();
    }
}
