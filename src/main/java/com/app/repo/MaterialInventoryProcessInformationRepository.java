package com.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MaterialInventoryProcessInformation;


/**
 * Spring Data  repository for the MaterialInventoryProcessInformation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialInventoryProcessInformationRepository extends JpaRepository<MaterialInventoryProcessInformation, Long> {

}
