package com.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MaterialQuantityConversion;


/**
 * Spring Data  repository for the MaterialQuantityConversion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialQuantityConversionRepository extends JpaRepository<MaterialQuantityConversion, Long> {

}
