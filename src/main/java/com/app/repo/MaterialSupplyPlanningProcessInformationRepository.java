package com.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MaterialSupplyPlanningProcessInformation;


/**
 * Spring Data  repository for the MaterialSupplyPlanningProcessInformation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialSupplyPlanningProcessInformationRepository extends JpaRepository<MaterialSupplyPlanningProcessInformation, Long> {

}
