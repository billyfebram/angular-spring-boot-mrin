package com.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MaterialDeviantTaxClassification;


/**
 * Spring Data  repository for the MaterialDeviantTaxClassification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialDeviantTaxClassificationRepository extends JpaRepository<MaterialDeviantTaxClassification, Long> {

}
