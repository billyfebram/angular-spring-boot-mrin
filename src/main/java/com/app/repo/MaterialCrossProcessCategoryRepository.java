package com.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MaterialCrossProcessCategory;


/**
 * Spring Data  repository for the MaterialCrossProcessCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialCrossProcessCategoryRepository extends JpaRepository<MaterialCrossProcessCategory, Long> {

}
