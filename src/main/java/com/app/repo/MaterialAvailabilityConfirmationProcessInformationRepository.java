package com.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MaterialAvailabilityConfirmationProcessInformation;


/**
 * Spring Data  repository for the MaterialAvailabilityConfirmationProcessInformation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialAvailabilityConfirmationProcessInformationRepository extends JpaRepository<MaterialAvailabilityConfirmationProcessInformation, Long> {

}
