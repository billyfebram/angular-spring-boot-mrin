package com.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MaterialSalesProcessInformation;


/**
 * Spring Data  repository for the MaterialSalesProcessInformation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialSalesProcessInformationRepository extends JpaRepository<MaterialSalesProcessInformation, Long> {

}
