package com.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MaterialFinancialProcessInformation;


/**
 * Spring Data  repository for the MaterialFinancialProcessInformation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialFinancialProcessInformationRepository extends JpaRepository<MaterialFinancialProcessInformation, Long> {

}
