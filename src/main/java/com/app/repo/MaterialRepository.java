package com.app.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.model.Material;

/**
 * Spring Data  repository for the Material entity.
 */
@Repository
public interface MaterialRepository extends JpaRepository<Material, Long> {

    @Query(value = "select distinct material from Material material left join fetch material.materialDeviantTaxClassifications left join fetch material.materialQuantityConversions left join fetch material.materialCrossProcessCategories left join fetch material.materialInventoryProcessInformations left join fetch material.materialSalesProcessInformations left join fetch material.materialFinancialProcessInformations left join fetch material.materialSupplyPlanningProcessInformations left join fetch material.materialAvailabilityConfirmationProcessInformations",
        countQuery = "select count(distinct material) from Material material")
    Page<Material> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct material from Material material left join fetch material.materialDeviantTaxClassifications left join fetch material.materialQuantityConversions left join fetch material.materialCrossProcessCategories left join fetch material.materialInventoryProcessInformations left join fetch material.materialSalesProcessInformations left join fetch material.materialFinancialProcessInformations left join fetch material.materialSupplyPlanningProcessInformations left join fetch material.materialAvailabilityConfirmationProcessInformations")
    List<Material> findAllWithEagerRelationships();

    @Query("select material from Material material left join fetch material.materialDeviantTaxClassifications left join fetch material.materialQuantityConversions left join fetch material.materialCrossProcessCategories left join fetch material.materialInventoryProcessInformations left join fetch material.materialSalesProcessInformations left join fetch material.materialFinancialProcessInformations left join fetch material.materialSupplyPlanningProcessInformations left join fetch material.materialAvailabilityConfirmationProcessInformations where material.id =:id")
    Optional<Material> findOneWithEagerRelationships(@Param("id") Long id);

}
