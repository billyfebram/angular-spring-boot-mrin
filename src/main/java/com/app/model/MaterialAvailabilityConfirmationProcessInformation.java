package com.app.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A MaterialAvailabilityConfirmationProcessInformation.
 */
@Entity
@Table(name = "m_availability_confirm_info")
public class MaterialAvailabilityConfirmationProcessInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "availability_confirmation_mode_code")
    private String availabilityConfirmationModeCode;

    @Column(name = "availability_confirmation_mode_code_text")
    private String availabilityConfirmationModeCodeText;

    @Column(name = "supply_planning_area_id")
    private String supplyPlanningAreaID;

    @Column(name = "life_cycle_status_code")
    private String lifeCycleStatusCode;

    @Column(name = "life_cycle_status_code_text")
    private String lifeCycleStatusCodeText;

    @ManyToMany(mappedBy = "materialAvailabilityConfirmationProcessInformations")
    @JsonIgnore
    private Set<Material> materials = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAvailabilityConfirmationModeCode() {
        return availabilityConfirmationModeCode;
    }

    public MaterialAvailabilityConfirmationProcessInformation availabilityConfirmationModeCode(String availabilityConfirmationModeCode) {
        this.availabilityConfirmationModeCode = availabilityConfirmationModeCode;
        return this;
    }

    public void setAvailabilityConfirmationModeCode(String availabilityConfirmationModeCode) {
        this.availabilityConfirmationModeCode = availabilityConfirmationModeCode;
    }

    public String getAvailabilityConfirmationModeCodeText() {
        return availabilityConfirmationModeCodeText;
    }

    public MaterialAvailabilityConfirmationProcessInformation availabilityConfirmationModeCodeText(String availabilityConfirmationModeCodeText) {
        this.availabilityConfirmationModeCodeText = availabilityConfirmationModeCodeText;
        return this;
    }

    public void setAvailabilityConfirmationModeCodeText(String availabilityConfirmationModeCodeText) {
        this.availabilityConfirmationModeCodeText = availabilityConfirmationModeCodeText;
    }

    public String getSupplyPlanningAreaID() {
        return supplyPlanningAreaID;
    }

    public MaterialAvailabilityConfirmationProcessInformation supplyPlanningAreaID(String supplyPlanningAreaID) {
        this.supplyPlanningAreaID = supplyPlanningAreaID;
        return this;
    }

    public void setSupplyPlanningAreaID(String supplyPlanningAreaID) {
        this.supplyPlanningAreaID = supplyPlanningAreaID;
    }

    public String getLifeCycleStatusCode() {
        return lifeCycleStatusCode;
    }

    public MaterialAvailabilityConfirmationProcessInformation lifeCycleStatusCode(String lifeCycleStatusCode) {
        this.lifeCycleStatusCode = lifeCycleStatusCode;
        return this;
    }

    public void setLifeCycleStatusCode(String lifeCycleStatusCode) {
        this.lifeCycleStatusCode = lifeCycleStatusCode;
    }

    public String getLifeCycleStatusCodeText() {
        return lifeCycleStatusCodeText;
    }

    public MaterialAvailabilityConfirmationProcessInformation lifeCycleStatusCodeText(String lifeCycleStatusCodeText) {
        this.lifeCycleStatusCodeText = lifeCycleStatusCodeText;
        return this;
    }

    public void setLifeCycleStatusCodeText(String lifeCycleStatusCodeText) {
        this.lifeCycleStatusCodeText = lifeCycleStatusCodeText;
    }

    public Set<Material> getMaterials() {
        return materials;
    }

    public MaterialAvailabilityConfirmationProcessInformation materials(Set<Material> materials) {
        this.materials = materials;
        return this;
    }

    public MaterialAvailabilityConfirmationProcessInformation addMaterial(Material material) {
        this.materials.add(material);
        material.getMaterialAvailabilityConfirmationProcessInformations().add(this);
        return this;
    }

    public MaterialAvailabilityConfirmationProcessInformation removeMaterial(Material material) {
        this.materials.remove(material);
        material.getMaterialAvailabilityConfirmationProcessInformations().remove(this);
        return this;
    }

    public void setMaterials(Set<Material> materials) {
        this.materials = materials;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MaterialAvailabilityConfirmationProcessInformation)) {
            return false;
        }
        return id != null && id.equals(((MaterialAvailabilityConfirmationProcessInformation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MaterialAvailabilityConfirmationProcessInformation{" +
            "id=" + getId() +
            ", availabilityConfirmationModeCode='" + getAvailabilityConfirmationModeCode() + "'" +
            ", availabilityConfirmationModeCodeText='" + getAvailabilityConfirmationModeCodeText() + "'" +
            ", supplyPlanningAreaID='" + getSupplyPlanningAreaID() + "'" +
            ", lifeCycleStatusCode='" + getLifeCycleStatusCode() + "'" +
            ", lifeCycleStatusCodeText='" + getLifeCycleStatusCodeText() + "'" +
            "}";
    }
}
