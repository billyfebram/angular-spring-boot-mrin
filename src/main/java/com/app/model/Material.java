package com.app.model;



import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Material.
 */
@Entity
@Table(name = "material")
public class Material implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "procurement_measure_unit_code")
    private String procurementMeasureUnitCode;

    @Column(name = "procurement_measure_unit_code_text")
    private String procurementMeasureUnitCodeText;

    @Column(name = "procurement_life_cycle_status_code")
    private String procurementLifeCycleStatusCode;

    @Column(name = "procurement_life_cycle_status_code_text")
    private String procurementLifeCycleStatusCodeText;

    @Column(name = "internal_id")
    private String internalID;

    @Column(name = "u_uid")
    private String uUID;

    @Column(name = "base_measure_unit_code")
    private String baseMeasureUnitCode;

    @Column(name = "base_measure_unit_code_text")
    private String baseMeasureUnitCodeText;

    @Column(name = "identified_stock_type_code")
    private String identifiedStockTypeCode;

    @Column(name = "identified_stock_type_code_text")
    private String identifiedStockTypeCodeText;

    @Column(name = "language_code")
    private String languageCode;

    @Column(name = "description")
    private String description;

    @Column(name = "language_code_text")
    private String languageCodeText;

    @Column(name = "product_valuation_level_type_code")
    private String productValuationLevelTypeCode;

    @Column(name = "product_valuation_level_type_code_text")
    private String productValuationLevelTypeCodeText;

    @ManyToMany
    @JoinTable(name = "material_material_deviant_tax_classification",
               joinColumns = @JoinColumn(name = "material_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "material_deviant_tax_classification_id", referencedColumnName = "id"))
    private Set<MaterialDeviantTaxClassification> materialDeviantTaxClassifications = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "material_material_quantity_conversion",
               joinColumns = @JoinColumn(name = "material_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "material_quantity_conversion_id", referencedColumnName = "id"))
    private Set<MaterialQuantityConversion> materialQuantityConversions = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "material_material_cross_process_category",
               joinColumns = @JoinColumn(name = "material_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "material_cross_process_category_id", referencedColumnName = "id"))
    private Set<MaterialCrossProcessCategory> materialCrossProcessCategories = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "material_material_inventory_process_information",
               joinColumns = @JoinColumn(name = "material_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "material_inventory_process_information_id", referencedColumnName = "id"))
    private Set<MaterialInventoryProcessInformation> materialInventoryProcessInformations = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "material_material_sales_process_information",
               joinColumns = @JoinColumn(name = "material_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "material_sales_process_information_id", referencedColumnName = "id"))
    private Set<MaterialSalesProcessInformation> materialSalesProcessInformations = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "material_material_financial_process_information",
               joinColumns = @JoinColumn(name = "material_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "material_financial_process_information_id", referencedColumnName = "id"))
    private Set<MaterialFinancialProcessInformation> materialFinancialProcessInformations = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "material_material_supply_planning_process_information",
               joinColumns = @JoinColumn(name = "material_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "material_supply_planning_process_information_id", referencedColumnName = "id"))
    private Set<MaterialSupplyPlanningProcessInformation> materialSupplyPlanningProcessInformations = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "material_material_availability_confirmation_process_information",
               joinColumns = @JoinColumn(name = "material_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "material_availability_confirmation_process_information_id", referencedColumnName = "id"))
    private Set<MaterialAvailabilityConfirmationProcessInformation> materialAvailabilityConfirmationProcessInformations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProcurementMeasureUnitCode() {
        return procurementMeasureUnitCode;
    }

    public Material procurementMeasureUnitCode(String procurementMeasureUnitCode) {
        this.procurementMeasureUnitCode = procurementMeasureUnitCode;
        return this;
    }

    public void setProcurementMeasureUnitCode(String procurementMeasureUnitCode) {
        this.procurementMeasureUnitCode = procurementMeasureUnitCode;
    }

    public String getProcurementMeasureUnitCodeText() {
        return procurementMeasureUnitCodeText;
    }

    public Material procurementMeasureUnitCodeText(String procurementMeasureUnitCodeText) {
        this.procurementMeasureUnitCodeText = procurementMeasureUnitCodeText;
        return this;
    }

    public void setProcurementMeasureUnitCodeText(String procurementMeasureUnitCodeText) {
        this.procurementMeasureUnitCodeText = procurementMeasureUnitCodeText;
    }

    public String getProcurementLifeCycleStatusCode() {
        return procurementLifeCycleStatusCode;
    }

    public Material procurementLifeCycleStatusCode(String procurementLifeCycleStatusCode) {
        this.procurementLifeCycleStatusCode = procurementLifeCycleStatusCode;
        return this;
    }

    public void setProcurementLifeCycleStatusCode(String procurementLifeCycleStatusCode) {
        this.procurementLifeCycleStatusCode = procurementLifeCycleStatusCode;
    }

    public String getProcurementLifeCycleStatusCodeText() {
        return procurementLifeCycleStatusCodeText;
    }

    public Material procurementLifeCycleStatusCodeText(String procurementLifeCycleStatusCodeText) {
        this.procurementLifeCycleStatusCodeText = procurementLifeCycleStatusCodeText;
        return this;
    }

    public void setProcurementLifeCycleStatusCodeText(String procurementLifeCycleStatusCodeText) {
        this.procurementLifeCycleStatusCodeText = procurementLifeCycleStatusCodeText;
    }

    public String getInternalID() {
        return internalID;
    }

    public Material internalID(String internalID) {
        this.internalID = internalID;
        return this;
    }

    public void setInternalID(String internalID) {
        this.internalID = internalID;
    }

    public String getuUID() {
        return uUID;
    }

    public Material uUID(String uUID) {
        this.uUID = uUID;
        return this;
    }

    public void setuUID(String uUID) {
        this.uUID = uUID;
    }

    public String getBaseMeasureUnitCode() {
        return baseMeasureUnitCode;
    }

    public Material baseMeasureUnitCode(String baseMeasureUnitCode) {
        this.baseMeasureUnitCode = baseMeasureUnitCode;
        return this;
    }

    public void setBaseMeasureUnitCode(String baseMeasureUnitCode) {
        this.baseMeasureUnitCode = baseMeasureUnitCode;
    }

    public String getBaseMeasureUnitCodeText() {
        return baseMeasureUnitCodeText;
    }

    public Material baseMeasureUnitCodeText(String baseMeasureUnitCodeText) {
        this.baseMeasureUnitCodeText = baseMeasureUnitCodeText;
        return this;
    }

    public void setBaseMeasureUnitCodeText(String baseMeasureUnitCodeText) {
        this.baseMeasureUnitCodeText = baseMeasureUnitCodeText;
    }

    public String getIdentifiedStockTypeCode() {
        return identifiedStockTypeCode;
    }

    public Material identifiedStockTypeCode(String identifiedStockTypeCode) {
        this.identifiedStockTypeCode = identifiedStockTypeCode;
        return this;
    }

    public void setIdentifiedStockTypeCode(String identifiedStockTypeCode) {
        this.identifiedStockTypeCode = identifiedStockTypeCode;
    }

    public String getIdentifiedStockTypeCodeText() {
        return identifiedStockTypeCodeText;
    }

    public Material identifiedStockTypeCodeText(String identifiedStockTypeCodeText) {
        this.identifiedStockTypeCodeText = identifiedStockTypeCodeText;
        return this;
    }

    public void setIdentifiedStockTypeCodeText(String identifiedStockTypeCodeText) {
        this.identifiedStockTypeCodeText = identifiedStockTypeCodeText;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public Material languageCode(String languageCode) {
        this.languageCode = languageCode;
        return this;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getDescription() {
        return description;
    }

    public Material description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguageCodeText() {
        return languageCodeText;
    }

    public Material languageCodeText(String languageCodeText) {
        this.languageCodeText = languageCodeText;
        return this;
    }

    public void setLanguageCodeText(String languageCodeText) {
        this.languageCodeText = languageCodeText;
    }

    public String getProductValuationLevelTypeCode() {
        return productValuationLevelTypeCode;
    }

    public Material productValuationLevelTypeCode(String productValuationLevelTypeCode) {
        this.productValuationLevelTypeCode = productValuationLevelTypeCode;
        return this;
    }

    public void setProductValuationLevelTypeCode(String productValuationLevelTypeCode) {
        this.productValuationLevelTypeCode = productValuationLevelTypeCode;
    }

    public String getProductValuationLevelTypeCodeText() {
        return productValuationLevelTypeCodeText;
    }

    public Material productValuationLevelTypeCodeText(String productValuationLevelTypeCodeText) {
        this.productValuationLevelTypeCodeText = productValuationLevelTypeCodeText;
        return this;
    }

    public void setProductValuationLevelTypeCodeText(String productValuationLevelTypeCodeText) {
        this.productValuationLevelTypeCodeText = productValuationLevelTypeCodeText;
    }

    public Set<MaterialDeviantTaxClassification> getMaterialDeviantTaxClassifications() {
        return materialDeviantTaxClassifications;
    }

    public Material materialDeviantTaxClassifications(Set<MaterialDeviantTaxClassification> materialDeviantTaxClassifications) {
        this.materialDeviantTaxClassifications = materialDeviantTaxClassifications;
        return this;
    }

    public Material addMaterialDeviantTaxClassification(MaterialDeviantTaxClassification materialDeviantTaxClassification) {
        this.materialDeviantTaxClassifications.add(materialDeviantTaxClassification);
        materialDeviantTaxClassification.getMaterials().add(this);
        return this;
    }

    public Material removeMaterialDeviantTaxClassification(MaterialDeviantTaxClassification materialDeviantTaxClassification) {
        this.materialDeviantTaxClassifications.remove(materialDeviantTaxClassification);
        materialDeviantTaxClassification.getMaterials().remove(this);
        return this;
    }

    public void setMaterialDeviantTaxClassifications(Set<MaterialDeviantTaxClassification> materialDeviantTaxClassifications) {
        this.materialDeviantTaxClassifications = materialDeviantTaxClassifications;
    }

    public Set<MaterialQuantityConversion> getMaterialQuantityConversions() {
        return materialQuantityConversions;
    }

    public Material materialQuantityConversions(Set<MaterialQuantityConversion> materialQuantityConversions) {
        this.materialQuantityConversions = materialQuantityConversions;
        return this;
    }

    public Material addMaterialQuantityConversion(MaterialQuantityConversion materialQuantityConversion) {
        this.materialQuantityConversions.add(materialQuantityConversion);
        materialQuantityConversion.getMaterials().add(this);
        return this;
    }

    public Material removeMaterialQuantityConversion(MaterialQuantityConversion materialQuantityConversion) {
        this.materialQuantityConversions.remove(materialQuantityConversion);
        materialQuantityConversion.getMaterials().remove(this);
        return this;
    }

    public void setMaterialQuantityConversions(Set<MaterialQuantityConversion> materialQuantityConversions) {
        this.materialQuantityConversions = materialQuantityConversions;
    }

    public Set<MaterialCrossProcessCategory> getMaterialCrossProcessCategories() {
        return materialCrossProcessCategories;
    }

    public Material materialCrossProcessCategories(Set<MaterialCrossProcessCategory> materialCrossProcessCategories) {
        this.materialCrossProcessCategories = materialCrossProcessCategories;
        return this;
    }

    public Material addMaterialCrossProcessCategory(MaterialCrossProcessCategory materialCrossProcessCategory) {
        this.materialCrossProcessCategories.add(materialCrossProcessCategory);
        materialCrossProcessCategory.getMaterials().add(this);
        return this;
    }

    public Material removeMaterialCrossProcessCategory(MaterialCrossProcessCategory materialCrossProcessCategory) {
        this.materialCrossProcessCategories.remove(materialCrossProcessCategory);
        materialCrossProcessCategory.getMaterials().remove(this);
        return this;
    }

    public void setMaterialCrossProcessCategories(Set<MaterialCrossProcessCategory> materialCrossProcessCategories) {
        this.materialCrossProcessCategories = materialCrossProcessCategories;
    }

    public Set<MaterialInventoryProcessInformation> getMaterialInventoryProcessInformations() {
        return materialInventoryProcessInformations;
    }

    public Material materialInventoryProcessInformations(Set<MaterialInventoryProcessInformation> materialInventoryProcessInformations) {
        this.materialInventoryProcessInformations = materialInventoryProcessInformations;
        return this;
    }

    public Material addMaterialInventoryProcessInformation(MaterialInventoryProcessInformation materialInventoryProcessInformation) {
        this.materialInventoryProcessInformations.add(materialInventoryProcessInformation);
        materialInventoryProcessInformation.getMaterials().add(this);
        return this;
    }

    public Material removeMaterialInventoryProcessInformation(MaterialInventoryProcessInformation materialInventoryProcessInformation) {
        this.materialInventoryProcessInformations.remove(materialInventoryProcessInformation);
        materialInventoryProcessInformation.getMaterials().remove(this);
        return this;
    }

    public void setMaterialInventoryProcessInformations(Set<MaterialInventoryProcessInformation> materialInventoryProcessInformations) {
        this.materialInventoryProcessInformations = materialInventoryProcessInformations;
    }

    public Set<MaterialSalesProcessInformation> getMaterialSalesProcessInformations() {
        return materialSalesProcessInformations;
    }

    public Material materialSalesProcessInformations(Set<MaterialSalesProcessInformation> materialSalesProcessInformations) {
        this.materialSalesProcessInformations = materialSalesProcessInformations;
        return this;
    }

    public Material addMaterialSalesProcessInformation(MaterialSalesProcessInformation materialSalesProcessInformation) {
        this.materialSalesProcessInformations.add(materialSalesProcessInformation);
        materialSalesProcessInformation.getMaterials().add(this);
        return this;
    }

    public Material removeMaterialSalesProcessInformation(MaterialSalesProcessInformation materialSalesProcessInformation) {
        this.materialSalesProcessInformations.remove(materialSalesProcessInformation);
        materialSalesProcessInformation.getMaterials().remove(this);
        return this;
    }

    public void setMaterialSalesProcessInformations(Set<MaterialSalesProcessInformation> materialSalesProcessInformations) {
        this.materialSalesProcessInformations = materialSalesProcessInformations;
    }

    public Set<MaterialFinancialProcessInformation> getMaterialFinancialProcessInformations() {
        return materialFinancialProcessInformations;
    }

    public Material materialFinancialProcessInformations(Set<MaterialFinancialProcessInformation> materialFinancialProcessInformations) {
        this.materialFinancialProcessInformations = materialFinancialProcessInformations;
        return this;
    }

    public Material addMaterialFinancialProcessInformation(MaterialFinancialProcessInformation materialFinancialProcessInformation) {
        this.materialFinancialProcessInformations.add(materialFinancialProcessInformation);
        materialFinancialProcessInformation.getMaterials().add(this);
        return this;
    }

    public Material removeMaterialFinancialProcessInformation(MaterialFinancialProcessInformation materialFinancialProcessInformation) {
        this.materialFinancialProcessInformations.remove(materialFinancialProcessInformation);
        materialFinancialProcessInformation.getMaterials().remove(this);
        return this;
    }

    public void setMaterialFinancialProcessInformations(Set<MaterialFinancialProcessInformation> materialFinancialProcessInformations) {
        this.materialFinancialProcessInformations = materialFinancialProcessInformations;
    }

    public Set<MaterialSupplyPlanningProcessInformation> getMaterialSupplyPlanningProcessInformations() {
        return materialSupplyPlanningProcessInformations;
    }

    public Material materialSupplyPlanningProcessInformations(Set<MaterialSupplyPlanningProcessInformation> materialSupplyPlanningProcessInformations) {
        this.materialSupplyPlanningProcessInformations = materialSupplyPlanningProcessInformations;
        return this;
    }

    public Material addMaterialSupplyPlanningProcessInformation(MaterialSupplyPlanningProcessInformation materialSupplyPlanningProcessInformation) {
        this.materialSupplyPlanningProcessInformations.add(materialSupplyPlanningProcessInformation);
        materialSupplyPlanningProcessInformation.getMaterials().add(this);
        return this;
    }

    public Material removeMaterialSupplyPlanningProcessInformation(MaterialSupplyPlanningProcessInformation materialSupplyPlanningProcessInformation) {
        this.materialSupplyPlanningProcessInformations.remove(materialSupplyPlanningProcessInformation);
        materialSupplyPlanningProcessInformation.getMaterials().remove(this);
        return this;
    }

    public void setMaterialSupplyPlanningProcessInformations(Set<MaterialSupplyPlanningProcessInformation> materialSupplyPlanningProcessInformations) {
        this.materialSupplyPlanningProcessInformations = materialSupplyPlanningProcessInformations;
    }

    public Set<MaterialAvailabilityConfirmationProcessInformation> getMaterialAvailabilityConfirmationProcessInformations() {
        return materialAvailabilityConfirmationProcessInformations;
    }

    public Material materialAvailabilityConfirmationProcessInformations(Set<MaterialAvailabilityConfirmationProcessInformation> materialAvailabilityConfirmationProcessInformations) {
        this.materialAvailabilityConfirmationProcessInformations = materialAvailabilityConfirmationProcessInformations;
        return this;
    }

    public Material addMaterialAvailabilityConfirmationProcessInformation(MaterialAvailabilityConfirmationProcessInformation materialAvailabilityConfirmationProcessInformation) {
        this.materialAvailabilityConfirmationProcessInformations.add(materialAvailabilityConfirmationProcessInformation);
        materialAvailabilityConfirmationProcessInformation.getMaterials().add(this);
        return this;
    }

    public Material removeMaterialAvailabilityConfirmationProcessInformation(MaterialAvailabilityConfirmationProcessInformation materialAvailabilityConfirmationProcessInformation) {
        this.materialAvailabilityConfirmationProcessInformations.remove(materialAvailabilityConfirmationProcessInformation);
        materialAvailabilityConfirmationProcessInformation.getMaterials().remove(this);
        return this;
    }

    public void setMaterialAvailabilityConfirmationProcessInformations(Set<MaterialAvailabilityConfirmationProcessInformation> materialAvailabilityConfirmationProcessInformations) {
        this.materialAvailabilityConfirmationProcessInformations = materialAvailabilityConfirmationProcessInformations;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Material)) {
            return false;
        }
        return id != null && id.equals(((Material) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Material{" +
            "id=" + getId() +
            ", procurementMeasureUnitCode='" + getProcurementMeasureUnitCode() + "'" +
            ", procurementMeasureUnitCodeText='" + getProcurementMeasureUnitCodeText() + "'" +
            ", procurementLifeCycleStatusCode='" + getProcurementLifeCycleStatusCode() + "'" +
            ", procurementLifeCycleStatusCodeText='" + getProcurementLifeCycleStatusCodeText() + "'" +
            ", internalID='" + getInternalID() + "'" +
            ", uUID='" + getuUID() + "'" +
            ", baseMeasureUnitCode='" + getBaseMeasureUnitCode() + "'" +
            ", baseMeasureUnitCodeText='" + getBaseMeasureUnitCodeText() + "'" +
            ", identifiedStockTypeCode='" + getIdentifiedStockTypeCode() + "'" +
            ", identifiedStockTypeCodeText='" + getIdentifiedStockTypeCodeText() + "'" +
            ", languageCode='" + getLanguageCode() + "'" +
            ", description='" + getDescription() + "'" +
            ", languageCodeText='" + getLanguageCodeText() + "'" +
            ", productValuationLevelTypeCode='" + getProductValuationLevelTypeCode() + "'" +
            ", productValuationLevelTypeCodeText='" + getProductValuationLevelTypeCodeText() + "'" +
            "}";
    }
}
