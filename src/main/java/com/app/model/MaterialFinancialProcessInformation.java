package com.app.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A MaterialFinancialProcessInformation.
 */
@Entity
@Table(name = "m_financial_process_info")
public class MaterialFinancialProcessInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "company_id")
    private String companyID;

    @Column(name = "permanent_establishment_id")
    private String permanentEstablishmentID;

    @Column(name = "life_cycle_status_code")
    private String lifeCycleStatusCode;

    @Column(name = "life_cycle_status_code_text")
    private String lifeCycleStatusCodeText;

    @ManyToMany(mappedBy = "materialFinancialProcessInformations")
    @JsonIgnore
    private Set<Material> materials = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyID() {
        return companyID;
    }

    public MaterialFinancialProcessInformation companyID(String companyID) {
        this.companyID = companyID;
        return this;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getPermanentEstablishmentID() {
        return permanentEstablishmentID;
    }

    public MaterialFinancialProcessInformation permanentEstablishmentID(String permanentEstablishmentID) {
        this.permanentEstablishmentID = permanentEstablishmentID;
        return this;
    }

    public void setPermanentEstablishmentID(String permanentEstablishmentID) {
        this.permanentEstablishmentID = permanentEstablishmentID;
    }

    public String getLifeCycleStatusCode() {
        return lifeCycleStatusCode;
    }

    public MaterialFinancialProcessInformation lifeCycleStatusCode(String lifeCycleStatusCode) {
        this.lifeCycleStatusCode = lifeCycleStatusCode;
        return this;
    }

    public void setLifeCycleStatusCode(String lifeCycleStatusCode) {
        this.lifeCycleStatusCode = lifeCycleStatusCode;
    }

    public String getLifeCycleStatusCodeText() {
        return lifeCycleStatusCodeText;
    }

    public MaterialFinancialProcessInformation lifeCycleStatusCodeText(String lifeCycleStatusCodeText) {
        this.lifeCycleStatusCodeText = lifeCycleStatusCodeText;
        return this;
    }

    public void setLifeCycleStatusCodeText(String lifeCycleStatusCodeText) {
        this.lifeCycleStatusCodeText = lifeCycleStatusCodeText;
    }

    public Set<Material> getMaterials() {
        return materials;
    }

    public MaterialFinancialProcessInformation materials(Set<Material> materials) {
        this.materials = materials;
        return this;
    }

    public MaterialFinancialProcessInformation addMaterial(Material material) {
        this.materials.add(material);
        material.getMaterialFinancialProcessInformations().add(this);
        return this;
    }

    public MaterialFinancialProcessInformation removeMaterial(Material material) {
        this.materials.remove(material);
        material.getMaterialFinancialProcessInformations().remove(this);
        return this;
    }

    public void setMaterials(Set<Material> materials) {
        this.materials = materials;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MaterialFinancialProcessInformation)) {
            return false;
        }
        return id != null && id.equals(((MaterialFinancialProcessInformation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MaterialFinancialProcessInformation{" +
            "id=" + getId() +
            ", companyID='" + getCompanyID() + "'" +
            ", permanentEstablishmentID='" + getPermanentEstablishmentID() + "'" +
            ", lifeCycleStatusCode='" + getLifeCycleStatusCode() + "'" +
            ", lifeCycleStatusCodeText='" + getLifeCycleStatusCodeText() + "'" +
            "}";
    }
}
