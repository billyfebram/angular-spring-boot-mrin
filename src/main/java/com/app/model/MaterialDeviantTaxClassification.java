package com.app.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A MaterialDeviantTaxClassification.
 */
@Entity
@Table(name = "m_deviant_tax_classification")
public class MaterialDeviantTaxClassification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "country_code")
    private String countryCode;

    @Column(name = "country_code_text")
    private String countryCodeText;

    @Column(name = "region_code")
    private String regionCode;

    @Column(name = "region_code_text")
    private String regionCodeText;

    @Column(name = "tax_exemption_reason_code")
    private String taxExemptionReasonCode;

    @Column(name = "tax_exemption_reason_code_text")
    private String taxExemptionReasonCodeText;

    @Column(name = "tax_rate_type_code")
    private String taxRateTypeCode;

    @Column(name = "tax_rate_type_code_text")
    private String taxRateTypeCodeText;

    @Column(name = "tax_type_code")
    private String taxTypeCode;

    @Column(name = "tax_type_code_text")
    private String taxTypeCodeText;

    @ManyToMany(mappedBy = "materialDeviantTaxClassifications")
    @JsonIgnore
    private Set<Material> materials = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public MaterialDeviantTaxClassification countryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCodeText() {
        return countryCodeText;
    }

    public MaterialDeviantTaxClassification countryCodeText(String countryCodeText) {
        this.countryCodeText = countryCodeText;
        return this;
    }

    public void setCountryCodeText(String countryCodeText) {
        this.countryCodeText = countryCodeText;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public MaterialDeviantTaxClassification regionCode(String regionCode) {
        this.regionCode = regionCode;
        return this;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCodeText() {
        return regionCodeText;
    }

    public MaterialDeviantTaxClassification regionCodeText(String regionCodeText) {
        this.regionCodeText = regionCodeText;
        return this;
    }

    public void setRegionCodeText(String regionCodeText) {
        this.regionCodeText = regionCodeText;
    }

    public String getTaxExemptionReasonCode() {
        return taxExemptionReasonCode;
    }

    public MaterialDeviantTaxClassification taxExemptionReasonCode(String taxExemptionReasonCode) {
        this.taxExemptionReasonCode = taxExemptionReasonCode;
        return this;
    }

    public void setTaxExemptionReasonCode(String taxExemptionReasonCode) {
        this.taxExemptionReasonCode = taxExemptionReasonCode;
    }

    public String getTaxExemptionReasonCodeText() {
        return taxExemptionReasonCodeText;
    }

    public MaterialDeviantTaxClassification taxExemptionReasonCodeText(String taxExemptionReasonCodeText) {
        this.taxExemptionReasonCodeText = taxExemptionReasonCodeText;
        return this;
    }

    public void setTaxExemptionReasonCodeText(String taxExemptionReasonCodeText) {
        this.taxExemptionReasonCodeText = taxExemptionReasonCodeText;
    }

    public String getTaxRateTypeCode() {
        return taxRateTypeCode;
    }

    public MaterialDeviantTaxClassification taxRateTypeCode(String taxRateTypeCode) {
        this.taxRateTypeCode = taxRateTypeCode;
        return this;
    }

    public void setTaxRateTypeCode(String taxRateTypeCode) {
        this.taxRateTypeCode = taxRateTypeCode;
    }

    public String getTaxRateTypeCodeText() {
        return taxRateTypeCodeText;
    }

    public MaterialDeviantTaxClassification taxRateTypeCodeText(String taxRateTypeCodeText) {
        this.taxRateTypeCodeText = taxRateTypeCodeText;
        return this;
    }

    public void setTaxRateTypeCodeText(String taxRateTypeCodeText) {
        this.taxRateTypeCodeText = taxRateTypeCodeText;
    }

    public String getTaxTypeCode() {
        return taxTypeCode;
    }

    public MaterialDeviantTaxClassification taxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
        return this;
    }

    public void setTaxTypeCode(String taxTypeCode) {
        this.taxTypeCode = taxTypeCode;
    }

    public String getTaxTypeCodeText() {
        return taxTypeCodeText;
    }

    public MaterialDeviantTaxClassification taxTypeCodeText(String taxTypeCodeText) {
        this.taxTypeCodeText = taxTypeCodeText;
        return this;
    }

    public void setTaxTypeCodeText(String taxTypeCodeText) {
        this.taxTypeCodeText = taxTypeCodeText;
    }

    public Set<Material> getMaterials() {
        return materials;
    }

    public MaterialDeviantTaxClassification materials(Set<Material> materials) {
        this.materials = materials;
        return this;
    }

    public MaterialDeviantTaxClassification addMaterial(Material material) {
        this.materials.add(material);
        material.getMaterialDeviantTaxClassifications().add(this);
        return this;
    }

    public MaterialDeviantTaxClassification removeMaterial(Material material) {
        this.materials.remove(material);
        material.getMaterialDeviantTaxClassifications().remove(this);
        return this;
    }

    public void setMaterials(Set<Material> materials) {
        this.materials = materials;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MaterialDeviantTaxClassification)) {
            return false;
        }
        return id != null && id.equals(((MaterialDeviantTaxClassification) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MaterialDeviantTaxClassification{" +
            "id=" + getId() +
            ", countryCode='" + getCountryCode() + "'" +
            ", countryCodeText='" + getCountryCodeText() + "'" +
            ", regionCode='" + getRegionCode() + "'" +
            ", regionCodeText='" + getRegionCodeText() + "'" +
            ", taxExemptionReasonCode='" + getTaxExemptionReasonCode() + "'" +
            ", taxExemptionReasonCodeText='" + getTaxExemptionReasonCodeText() + "'" +
            ", taxRateTypeCode='" + getTaxRateTypeCode() + "'" +
            ", taxRateTypeCodeText='" + getTaxRateTypeCodeText() + "'" +
            ", taxTypeCode='" + getTaxTypeCode() + "'" +
            ", taxTypeCodeText='" + getTaxTypeCodeText() + "'" +
            "}";
    }
}
