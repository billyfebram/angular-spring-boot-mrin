package com.app.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.app.model.MaterialQuantityConversion;

/**
 * Service Interface for managing {@link MaterialQuantityConversion}.
 */
public interface MaterialQuantityConversionService {

    /**
     * Save a materialQuantityConversion.
     *
     * @param materialQuantityConversion the entity to save.
     * @return the persisted entity.
     */
    MaterialQuantityConversion save(MaterialQuantityConversion materialQuantityConversion);

    /**
     * Get all the materialQuantityConversions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MaterialQuantityConversion> findAll(Pageable pageable);


    /**
     * Get the "id" materialQuantityConversion.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MaterialQuantityConversion> findOne(Long id);

    /**
     * Delete the "id" materialQuantityConversion.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
