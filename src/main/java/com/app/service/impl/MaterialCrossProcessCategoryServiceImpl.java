package com.app.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.model.MaterialCrossProcessCategory;
import com.app.repo.MaterialCrossProcessCategoryRepository;
import com.app.service.MaterialCrossProcessCategoryService;

/**
 * Service Implementation for managing {@link MaterialCrossProcessCategory}.
 */
@Service
@Transactional
public class MaterialCrossProcessCategoryServiceImpl implements MaterialCrossProcessCategoryService {

    private final Logger log = LoggerFactory.getLogger(MaterialCrossProcessCategoryServiceImpl.class);

    private final MaterialCrossProcessCategoryRepository materialCrossProcessCategoryRepository;

    public MaterialCrossProcessCategoryServiceImpl(MaterialCrossProcessCategoryRepository materialCrossProcessCategoryRepository) {
        this.materialCrossProcessCategoryRepository = materialCrossProcessCategoryRepository;
    }

    /**
     * Save a materialCrossProcessCategory.
     *
     * @param materialCrossProcessCategory the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MaterialCrossProcessCategory save(MaterialCrossProcessCategory materialCrossProcessCategory) {
        log.debug("Request to save MaterialCrossProcessCategory : {}", materialCrossProcessCategory);
        return materialCrossProcessCategoryRepository.save(materialCrossProcessCategory);
    }

    /**
     * Get all the materialCrossProcessCategories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MaterialCrossProcessCategory> findAll(Pageable pageable) {
        log.debug("Request to get all MaterialCrossProcessCategories");
        return materialCrossProcessCategoryRepository.findAll(pageable);
    }


    /**
     * Get one materialCrossProcessCategory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MaterialCrossProcessCategory> findOne(Long id) {
        log.debug("Request to get MaterialCrossProcessCategory : {}", id);
        return materialCrossProcessCategoryRepository.findById(id);
    }

    /**
     * Delete the materialCrossProcessCategory by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MaterialCrossProcessCategory : {}", id);
        materialCrossProcessCategoryRepository.deleteById(id);
    }
}
