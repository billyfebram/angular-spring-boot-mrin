package com.app.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.model.MaterialFinancialProcessInformation;
import com.app.repo.MaterialFinancialProcessInformationRepository;
import com.app.service.MaterialFinancialProcessInformationService;

/**
 * Service Implementation for managing {@link MaterialFinancialProcessInformation}.
 */
@Service
@Transactional
public class MaterialFinancialProcessInformationServiceImpl implements MaterialFinancialProcessInformationService {

    private final Logger log = LoggerFactory.getLogger(MaterialFinancialProcessInformationServiceImpl.class);

    private final MaterialFinancialProcessInformationRepository materialFinancialProcessInformationRepository;

    public MaterialFinancialProcessInformationServiceImpl(MaterialFinancialProcessInformationRepository materialFinancialProcessInformationRepository) {
        this.materialFinancialProcessInformationRepository = materialFinancialProcessInformationRepository;
    }

    /**
     * Save a materialFinancialProcessInformation.
     *
     * @param materialFinancialProcessInformation the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MaterialFinancialProcessInformation save(MaterialFinancialProcessInformation materialFinancialProcessInformation) {
        log.debug("Request to save MaterialFinancialProcessInformation : {}", materialFinancialProcessInformation);
        return materialFinancialProcessInformationRepository.save(materialFinancialProcessInformation);
    }

    /**
     * Get all the materialFinancialProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MaterialFinancialProcessInformation> findAll(Pageable pageable) {
        log.debug("Request to get all MaterialFinancialProcessInformations");
        return materialFinancialProcessInformationRepository.findAll(pageable);
    }


    /**
     * Get one materialFinancialProcessInformation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MaterialFinancialProcessInformation> findOne(Long id) {
        log.debug("Request to get MaterialFinancialProcessInformation : {}", id);
        return materialFinancialProcessInformationRepository.findById(id);
    }

    /**
     * Delete the materialFinancialProcessInformation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MaterialFinancialProcessInformation : {}", id);
        materialFinancialProcessInformationRepository.deleteById(id);
    }
}
