package com.app.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.model.MaterialDeviantTaxClassification;
import com.app.repo.MaterialDeviantTaxClassificationRepository;
import com.app.service.MaterialDeviantTaxClassificationService;

/**
 * Service Implementation for managing {@link MaterialDeviantTaxClassification}.
 */
@Service
@Transactional
public class MaterialDeviantTaxClassificationServiceImpl implements MaterialDeviantTaxClassificationService {

    private final Logger log = LoggerFactory.getLogger(MaterialDeviantTaxClassificationServiceImpl.class);

    private final MaterialDeviantTaxClassificationRepository materialDeviantTaxClassificationRepository;

    public MaterialDeviantTaxClassificationServiceImpl(MaterialDeviantTaxClassificationRepository materialDeviantTaxClassificationRepository) {
        this.materialDeviantTaxClassificationRepository = materialDeviantTaxClassificationRepository;
    }

    /**
     * Save a materialDeviantTaxClassification.
     *
     * @param materialDeviantTaxClassification the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MaterialDeviantTaxClassification save(MaterialDeviantTaxClassification materialDeviantTaxClassification) {
        log.debug("Request to save MaterialDeviantTaxClassification : {}", materialDeviantTaxClassification);
        return materialDeviantTaxClassificationRepository.save(materialDeviantTaxClassification);
    }

    /**
     * Get all the materialDeviantTaxClassifications.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MaterialDeviantTaxClassification> findAll(Pageable pageable) {
        log.debug("Request to get all MaterialDeviantTaxClassifications");
        return materialDeviantTaxClassificationRepository.findAll(pageable);
    }


    /**
     * Get one materialDeviantTaxClassification by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MaterialDeviantTaxClassification> findOne(Long id) {
        log.debug("Request to get MaterialDeviantTaxClassification : {}", id);
        return materialDeviantTaxClassificationRepository.findById(id);
    }

    /**
     * Delete the materialDeviantTaxClassification by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MaterialDeviantTaxClassification : {}", id);
        materialDeviantTaxClassificationRepository.deleteById(id);
    }
}
