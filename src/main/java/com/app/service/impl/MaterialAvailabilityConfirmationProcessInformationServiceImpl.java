package com.app.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.model.MaterialAvailabilityConfirmationProcessInformation;
import com.app.repo.MaterialAvailabilityConfirmationProcessInformationRepository;
import com.app.service.MaterialAvailabilityConfirmationProcessInformationService;

/**
 * Service Implementation for managing {@link MaterialAvailabilityConfirmationProcessInformation}.
 */
@Service
@Transactional
public class MaterialAvailabilityConfirmationProcessInformationServiceImpl implements MaterialAvailabilityConfirmationProcessInformationService {

    private final Logger log = LoggerFactory.getLogger(MaterialAvailabilityConfirmationProcessInformationServiceImpl.class);

    private final MaterialAvailabilityConfirmationProcessInformationRepository materialAvailabilityConfirmationProcessInformationRepository;

    public MaterialAvailabilityConfirmationProcessInformationServiceImpl(MaterialAvailabilityConfirmationProcessInformationRepository materialAvailabilityConfirmationProcessInformationRepository) {
        this.materialAvailabilityConfirmationProcessInformationRepository = materialAvailabilityConfirmationProcessInformationRepository;
    }

    /**
     * Save a materialAvailabilityConfirmationProcessInformation.
     *
     * @param materialAvailabilityConfirmationProcessInformation the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MaterialAvailabilityConfirmationProcessInformation save(MaterialAvailabilityConfirmationProcessInformation materialAvailabilityConfirmationProcessInformation) {
        log.debug("Request to save MaterialAvailabilityConfirmationProcessInformation : {}", materialAvailabilityConfirmationProcessInformation);
        return materialAvailabilityConfirmationProcessInformationRepository.save(materialAvailabilityConfirmationProcessInformation);
    }

    /**
     * Get all the materialAvailabilityConfirmationProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MaterialAvailabilityConfirmationProcessInformation> findAll(Pageable pageable) {
        log.debug("Request to get all MaterialAvailabilityConfirmationProcessInformations");
        return materialAvailabilityConfirmationProcessInformationRepository.findAll(pageable);
    }


    /**
     * Get one materialAvailabilityConfirmationProcessInformation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MaterialAvailabilityConfirmationProcessInformation> findOne(Long id) {
        log.debug("Request to get MaterialAvailabilityConfirmationProcessInformation : {}", id);
        return materialAvailabilityConfirmationProcessInformationRepository.findById(id);
    }

    /**
     * Delete the materialAvailabilityConfirmationProcessInformation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MaterialAvailabilityConfirmationProcessInformation : {}", id);
        materialAvailabilityConfirmationProcessInformationRepository.deleteById(id);
    }
}
