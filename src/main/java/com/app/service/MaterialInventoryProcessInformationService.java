package com.app.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.app.model.MaterialInventoryProcessInformation;

/**
 * Service Interface for managing {@link MaterialInventoryProcessInformation}.
 */
public interface MaterialInventoryProcessInformationService {

    /**
     * Save a materialInventoryProcessInformation.
     *
     * @param materialInventoryProcessInformation the entity to save.
     * @return the persisted entity.
     */
    MaterialInventoryProcessInformation save(MaterialInventoryProcessInformation materialInventoryProcessInformation);

    /**
     * Get all the materialInventoryProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MaterialInventoryProcessInformation> findAll(Pageable pageable);


    /**
     * Get the "id" materialInventoryProcessInformation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MaterialInventoryProcessInformation> findOne(Long id);

    /**
     * Delete the "id" materialInventoryProcessInformation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
