package com.app.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.app.model.MaterialSupplyPlanningProcessInformation;

/**
 * Service Interface for managing {@link MaterialSupplyPlanningProcessInformation}.
 */
public interface MaterialSupplyPlanningProcessInformationService {

    /**
     * Save a materialSupplyPlanningProcessInformation.
     *
     * @param materialSupplyPlanningProcessInformation the entity to save.
     * @return the persisted entity.
     */
    MaterialSupplyPlanningProcessInformation save(MaterialSupplyPlanningProcessInformation materialSupplyPlanningProcessInformation);

    /**
     * Get all the materialSupplyPlanningProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MaterialSupplyPlanningProcessInformation> findAll(Pageable pageable);


    /**
     * Get the "id" materialSupplyPlanningProcessInformation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MaterialSupplyPlanningProcessInformation> findOne(Long id);

    /**
     * Delete the "id" materialSupplyPlanningProcessInformation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
